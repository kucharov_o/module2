package com.epam.esm.controller;

import com.epam.esm.entity.Tag;
import com.epam.esm.payload.ApiResponse;
import com.epam.esm.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@RequestMapping("/api/tag")
public class TagController {
    @Autowired
    TagService tagService;

    @GetMapping("/{id}")
    public HttpEntity<?> getOneById(@PathVariable Integer id) throws SQLException, ClassNotFoundException {
        ApiResponse oneById = tagService.getOneById(id);
        return ResponseEntity.status(oneById.isSuccess() ? 200 : 404).body(oneById);
    }

    @GetMapping
    public HttpEntity<?> getAll() throws SQLException, ClassNotFoundException {
        ApiResponse all = tagService.getAll();
        return ResponseEntity.status(all.isSuccess() ? 200 : 409).body(all);
    }

    @PostMapping
    public HttpEntity<?> add(@RequestBody Tag tag) throws SQLException, ClassNotFoundException {
        ApiResponse add = tagService.add(tag);
        return ResponseEntity.status(add.isSuccess() ? 201 : 409).body(add);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> update(@PathVariable Integer id, @RequestBody Tag tag) throws SQLException, ClassNotFoundException {
        ApiResponse apiResponse = tagService.updateById(id, tag);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 404).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteById(@PathVariable Integer id) throws SQLException, ClassNotFoundException {
        ApiResponse apiResponse = tagService.deleteById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 404).body(apiResponse);
    }
}
