package com.epam.esm.controller;

import com.epam.esm.payload.ApiResponse;
import com.epam.esm.service.GiftCertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@Controller
public class GiftCertificateController {
    @Autowired
    GiftCertificateService certificateService;

//    @RequestMapping(value = "/api/giftCertificate/{id}", method = RequestMethod.GET)
    @GetMapping("/{id}")
    public HttpEntity<?> getOneById(@PathVariable Integer id) throws SQLException, ClassNotFoundException {
        ApiResponse oneById = certificateService.getOneById(id);
        return ResponseEntity.status(oneById.isSuccess() ? 200 : 404).body(oneById);
    }

//    @RequestMapping(value = "/api/giftCertificate", method = RequestMethod.GET)
    @GetMapping
    public HttpEntity<?> getAll() throws SQLException, ClassNotFoundException {
        ApiResponse all = certificateService.getAll();
        return ResponseEntity.status(all.isSuccess() ? 200 : 404).body(all);
    }

    //NO ADD AND UPDATE METHOD (NOT FINISHED)


    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteById(@PathVariable Integer id) throws SQLException, ClassNotFoundException {
        ApiResponse apiResponse = certificateService.deleteById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 404).body(apiResponse);
    }
}
