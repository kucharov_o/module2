package com.epam.esm.configurations;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class DbConfig {
    static final String url = "jdbc:postgresql://localhost:5432/Spring-JPA";
    static final String dbUser = "postgres";
    static final String dbPassword = "1234";

    public static Connection getConnection() throws SQLException, ClassNotFoundException {

        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
        return connection;
    }
}
