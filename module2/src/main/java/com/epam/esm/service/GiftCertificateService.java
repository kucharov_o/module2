package com.epam.esm.service;

import com.epam.esm.configurations.DbConfig;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.payload.ApiResponse;
import com.epam.esm.payload.GiftCertificateDto;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class GiftCertificateService {
    /**
     * GET ONE BY ID Method for Gift Certificate Entity
     * @param id
     * @return Gift Certificate
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ApiResponse getOneById(Integer id) throws SQLException, ClassNotFoundException {
        String sql = "select * from gift_certificate where gift_certificate.id=" + id;
        Connection connection = DbConfig.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        resultSet.next();
        if (!resultSet.next())
            return new ApiResponse("Gift Certificate NOT FOUND", false);

        GiftCertificate giftCertificate = new GiftCertificate(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
                resultSet.getInt(4), resultSet.getInt(5), resultSet.getString(6), resultSet.getString(7));
        return new ApiResponse("Success", true, giftCertificate);
    }

    /**
     * GET ALL Method for Gift Certificate Entity
     * @return List of Gift Certificates
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ApiResponse getAll() throws SQLException, ClassNotFoundException {
        String sql = "select * from gift_certificate";
        Connection connection = DbConfig.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        List<GiftCertificate> certificates = new ArrayList<>();

        if (!resultSet.next()) {
            return new ApiResponse("There is no any Gift Certificate", false);
        }

        while (resultSet.next()) {
            ResultSet resultSet1 = statement.executeQuery("select t.id, t.name from certificates_tags c join tags t on c.tag_id = t.id where certificate_id = " + resultSet.getInt("id"));
            List<Tag> tags = new ArrayList<>();
            while (resultSet1.next()) {
                tags.add(new Tag(resultSet1.getInt(1), resultSet1.getString(2)));
            }
            certificates.add(new GiftCertificate(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
                    resultSet.getInt(4), resultSet.getInt(5), resultSet.getString(6), resultSet.getString(7), tags));
        }
        return new ApiResponse("Success", true, certificates);
    }

    /**
     * ADD Method (POST) for Gift Certificate Entity
     * @param certificate
     * @return ApiResponse with Message and Success(boolean)
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ApiResponse addCertificate(GiftCertificateDto certificate) throws SQLException, ClassNotFoundException {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String createDate = timestamp.toString();
        String lastUpdateDate = timestamp.toString();

        String queryInsertGiftCertificate = "insert into gift_certificate(name, description, price, duration, create_date, last_update_date, tags)" +
                "values ('" + certificate.getName() + "','" + certificate.getDescription() + "','" + certificate.getPrice() + "','" + certificate.getDuration() + "','"
                + createDate + "','" + lastUpdateDate + "');";
        Connection connection = DbConfig.getConnection();
        Statement statement = connection.createStatement();
        statement.executeQuery(queryInsertGiftCertificate);
        for (Integer tag : certificate.getTags()) {
        String queryInsertCertificateTags = "insert into gift_certificate_tag(gift_certificate_id, tag_id)" +
                "values ( "+getAddedGiftCertificateId(certificate.getName())+", "+tag+")";
            statement.execute(queryInsertCertificateTags);
        }
        return new ApiResponse("Gift Certificate created successfully", true);
    }

    /**
     * Additional Method for ADD Method (POST) of Gift Certificate Entity
     * @param name
     * @return Integer Id of Added Gift Certificate for adding to gift_certificate_tag table
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public Integer getAddedGiftCertificateId(String name) throws SQLException, ClassNotFoundException {
        String query = "Select gift_certificate.id from gift_certificate where gift_certificate.name = "+name;
        Connection connection = DbConfig.getConnection();
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        Integer id = resultSet.getInt(1);
        return id;
    }

    /**
     * UPDATE (PUT) Method for Gift Certificate Entity
     * @param id
     * @param certificateDto
     * @return ApiResponse with Message and Success(boolean)
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ApiResponse updateCertificate(Integer id, GiftCertificateDto certificateDto) throws SQLException, ClassNotFoundException {
        String checkingSql = "select * from gift_certificate where gift_certificate.id" + id;
        Statement statement = DbConfig.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(checkingSql);
        if (!resultSet.next()) {
            return new ApiResponse("Gift Certificate NOT FOUND", false);
        }

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String lastUpdateDate = timestamp.toString();
        String updateQuery = "update gift_certificate set name = '" + certificateDto.getName() + "', description = '" + certificateDto.getDescription()
                + "', price = " + certificateDto.getPrice() + ", duration = " + certificateDto.getDuration() + ", last_update_date = " + lastUpdateDate
                + " where gift_certificate.id=" + id;
        statement.executeQuery(updateQuery);
        return new ApiResponse("Gift Certificate updated successfully", true);
    }

    /**
     * DELETE BY ID Method of Gift Certificate Entity
     * @param id
     * @return ApiResponse with Message and Success(boolean)
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ApiResponse deleteById(Integer id) throws SQLException, ClassNotFoundException {
        String sql = "select * from gift_certificate where gift_certificate.id=" + id;
        Connection connection = DbConfig.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        if (!resultSet.next())
            return new ApiResponse("Gift Certificate NOT FOUND", false);

        String deleteSql = "delete from gift_certificate where gift_certificate.id=" + id;
        ResultSet resultSetDelete = statement.executeQuery(deleteSql);
        resultSetDelete.next();
        return new ApiResponse("Row deleted successfully", true);
    }
}
