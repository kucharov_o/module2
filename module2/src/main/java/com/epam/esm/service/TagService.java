package com.epam.esm.service;

import com.epam.esm.configurations.DbConfig;
import com.epam.esm.entity.Tag;
import com.epam.esm.payload.ApiResponse;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Service
public class TagService {
    /**
     * GET ONE BY ID Method for Tag Entity
     * @param id
     * @return Api Response with Message Success and TAG(as object)
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ApiResponse getOneById(Integer id) throws SQLException, ClassNotFoundException {
        String sql = "select * from tag where tag.id=" + id;
        Connection connection = DbConfig.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        if (!resultSet.next())
            return new ApiResponse("Tag NOT FOUND", false);

        Tag tag = new Tag(resultSet.getInt(1), resultSet.getString(2));
        return new ApiResponse("success", true, tag);
    }

    /**
     * GET ALL Method for Tag Entity
     * @return ApiResponse with List of Tag
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ApiResponse getAll() throws SQLException, ClassNotFoundException {
        String sql = "select * from tag";
        Connection connection = DbConfig.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        List<Tag> tagList = new ArrayList<>();
        if (!resultSet.next())
            return new ApiResponse("There is no any Tags", false);
        while (resultSet.next()) {
            ResultSet resultSetTag = statement.executeQuery("select * from tag where tag.id=" + resultSet.getInt("id"));
            tagList.add(new Tag(resultSetTag.getInt(1), resultSetTag.getString(2)));
        }
        return new ApiResponse("Success", true, tagList);
    }

    /**
     * ADD (POST) Method for Tag Entity
     * @param tag
     * @return ApiResponse with Message and Success
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ApiResponse add(Tag tag) throws SQLException, ClassNotFoundException {
        String sql = "insert into tag(name) values('" + tag.getName() + "')";
        Connection connection = DbConfig.getConnection();
        Statement statement = connection.createStatement();
        statement.executeQuery(sql);
        return new ApiResponse("Tag added successfully", true);
    }

    /**
     * UPDATE (PUT) Method for Tag Entity
     * @param id
     * @param tag
     * @return ApiResponse with Message and Success
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ApiResponse updateById(Integer id, Tag tag) throws SQLException, ClassNotFoundException {
        String sql = "select * from tag where tag.id=" + id;
        Connection connection = DbConfig.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        if (!resultSet.next())
            return new ApiResponse("Tag NOT FOUND", false);

        String updateQuery = "update tag set name='" + tag.getName() + "' where tag.id=" + id;
        statement.executeQuery(updateQuery);
        return new ApiResponse("Tag updated successfully", true);
    }

    /**
     * DELETE BY ID Method for Tag Entity
     * @param id
     * @return ApiResponse with Message and Success
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ApiResponse deleteById(Integer id) throws SQLException, ClassNotFoundException {
        String sql = "select * from tag where tag.id=" + id;
        Connection connection = DbConfig.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        if (!resultSet.next())
            return new ApiResponse("Tag NOT FOUND", false);
        String deleteQuery = "delete from tag where tag.id=" + id;
        statement.executeQuery(deleteQuery);
        return new ApiResponse("Tag deleted successfully", true);
    }
}
