package com.epam.esm.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GiftCertificateDto {
    private Integer id;
    private String name;
    private String description;
    private Integer price;
    private Integer duration;
    private List<Integer> tags = new ArrayList<>();
}
